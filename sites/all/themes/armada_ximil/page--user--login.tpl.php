<?php
// $title_login="Ingresa con tu Cuenta Institucional.";
$title_login="INGRESA CON TU CUENTA INSTITUCIONAL";
?>
  <div >
    <div>
    <div>
    <div>
      <div> 
        <div>
             <div id="logo-floater">
          <?php if ($logo || $site_title): ?>
            <?php if ($title): ?>
              <div id=""><strong><a href="<?php print $front_page ?>">
              <?php if ($logo): ?>
                <img src="<?php print $logo ?>" alt="<?php print $site_name_and_slogan ?>" title="<?php print $site_name_and_slogan ?>" id="logo" style="margin-top: 1rem; text-align: center;" />
              <?php endif; ?>
              <?php //print $site_html ?>
              </a></strong></div>
            <?php else: /* Use h1 when the content title is empty */ ?>
              <h1 id="branding"><a href="<?php print $front_page ?>">
              <?php if ($logo): ?>
                <img src="<?php print $logo ?>" alt="<?php print $site_name_and_slogan ?>" title="<?php print $site_name_and_slogan ?>" id="logo" />
              <?php endif; ?>
              <?php //print $site_html ?>
              </a></h1>
          <?php endif; ?>
          <?php endif; ?>
          </div>
        </div>
      </div>
  </div>
<div class="bottom-header-outer"> 
    <div > 
      <div class="wrapper">
      <?php print render($page['header']); ?>
      </div>
    </div>
</div>

  <div class="slider">
    <div class="wrapper"><?php print render($page['slider']); ?></div>
  </div>
</div>
  </div>
    <div id="container" class="clearfix container-login">
  <div class="wrapper">
      <?php if ($page['sidebar_first']): ?>
        <div id="sidebar-first" class="sidebar">
          <?php print render($page['sidebar_first']); ?>
        </div>
      <?php endif; ?>
      <CENTER>
      <div id="center-login"><div id="squeeze"><div class="right-corner">
        <div class="left-corner">
          <!--<?php //print $breadcrumb; ?>-->
          <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
          <a id="main-content"></a>
          <?php if ($tabs): ?><div id="tabs-wrapper" class="clearfix"><?php endif; ?>
          <!--<?php //print render($title_prefix); ?>-->
          <?php if ($title): ?>
          <h1<?php print $tabs ? ' class="titles-login"' : '' ?> style="width: 22em; position: relative; bottom: 2rem; font-size: 34px;"><?php print $title_login ?></h1>
          <?php endif; ?>      
          <?php print render($title_suffix); ?>
          <?php if ($tabs): ?><?php print render($tabs); ?></div><?php endif; ?>
          <?php print render($tabs2); ?>
          <?php print $messages; ?>
          <?php print render($page['help']); ?>
          <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
          <div class="login-uno">
            <!-- <div>Si no tiene cuenta de correo institucional ingrese <a href="/createAccountZimbra/authentication/siath"><button>Aqui</button></a></div>  -->   
              <?php print render($page['content']); ?>
          </div>  
      </div>
    </div>
  </div >
      <div class="footer-login">
        <div class="container" style="text-align: center;">
          <div class="row" style="">
            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4" style="float: left; position: relative; top: 8px; right: 20px; margin-top: 2px;">ARMADA NACIONAL REPÙBLICA DE COLOMBIA</div>
            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4" style="float: left;">
              <img SRC="/sites/all/themes/armada_ximil/images/escudo-armada.png" width="30px">
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4" style="float: left; text-align: right; position: relative; top: 10px; left: 16px;">TODOS LOS DERECHOS RESERVADOS © 2017</div>
          </div>
        </div>
      </div>
<!-- </CENTER>/.left-corner, /.right-corner, /#squeeze, /#center -->

     <!--  <?php if ($page['sidebar_second']): ?>
        <div id="sidebar-second" class="sidebar">
          <?php print render($page['sidebar_second']); ?>
        </div>
      <?php endif; ?> -->
</div>
    </div> <!-- /#container -->
<!--   <div class="footer-outer">
    <div class="wrapper">   
    </div>
  </div> -->
  </div> <!-- /#wrapper -->