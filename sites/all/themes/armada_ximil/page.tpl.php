<?php
?>


  <div id="outer">
      <div class="bg-outer">
    <div class="dotted-bg">
		<div class="header-top">
		<div class="wrapper">
			<div class="header-section">
				   <div id="logo-floater">
        <?php if ($logo || $site_title): ?>
          <?php if ($title): ?>
            <div id="branding"><strong><a href="<?php print $front_page ?>">
            <?php if ($logo): ?>
              <img src="<?php print $logo ?>" alt="<?php print $site_name_and_slogan ?>" title="<?php print $site_name_and_slogan ?>" id="logo" />
            <?php endif; ?>
            <?php //print $site_html ?>
            </a></strong></div>
          <?php else: /* Use h1 when the content title is empty */ ?>
            <h1 id="branding"><a href="<?php print $front_page ?>">
            <?php if ($logo): ?>
              <img src="<?php print $logo ?>" alt="<?php print $site_name_and_slogan ?>" title="<?php print $site_name_and_slogan ?>" id="logo" />
            <?php endif; ?>
            <?php //print $site_html ?>
            </a></h1>
        <?php endif; ?>
        <?php endif; ?>
        </div>
			</div>
			
		</div>
		
	</div>
		<div class="bottom-header-outer"> 
		<div class="bottom-header"> 
			<div class="wrapper">
			<?php print render($page['header']); ?>
			</div>
		</div>
	</div>

  <div class="slider">
    <div class="wrapper"><?php print render($page['slider']); ?></div>
    <div class="wrapper2"><div class="tabber-container"><?php print render($page['tabbing_section']); ?></div></div>
  </div>
</div>
  </div>
    <div id="container" class="clearfix">
	<div class="wrapper">
      <?php if ($page['sidebar_first']): ?>
        <div id="sidebar-first" class="sidebar">
          <?php print render($page['sidebar_first']); ?>
        </div>
      <?php endif; ?>

      <div id="center"><div id="squeeze"><div class="right-corner"><div class="left-corner">
          <?php print $breadcrumb; ?>
          <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
          <a id="main-content"></a>
          <?php if ($tabs): ?><div id="tabs-wrapper" class="clearfix"><?php endif; ?>
          <?php print render($title_prefix); ?>
          <?php if ($title): ?>
            <h1<?php print $tabs ? ' class="with-tabs"' : '' ?>><?php print $title ?></h1>
          <?php endif; ?>
          <?php print render($title_suffix); ?>
          <?php if ($tabs): ?><?php print render($tabs); ?></div><?php endif; ?>
          <?php print render($tabs2); ?>
          <?php print $messages; ?>
          <?php print render($page['help']); ?>
          <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
          <div class="clearfix">
            <?php print render($page['content']); ?>
          </div>
         
         
      </div></div></div></div> <!-- /.left-corner, /.right-corner, /#squeeze, /#center -->

      <?php if ($page['sidebar_second']): ?>
        <div id="sidebar-second" class="sidebar">
          <?php print render($page['sidebar_second']); ?>
        </div>
      <?php endif; ?>
</div>
    </div> <!-- /#container -->
	<div class="footer-outer">
		<div class="wrapper">		
			<div class="shadow">
				 <?php print render($page['footer']); ?>
			</div>	 
		</div>
	</div>
  </div> <!-- /#wrapper -->
