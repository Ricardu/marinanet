$data = array(
  'bundles' => array(
    'inventario_prendas' => (object) array(
      'type' => 'inventario_prendas',
      'name' => 'Inventario Prendas',
      'base' => 'node_content',
      'module' => 'node',
      'description' => '',
      'help' => '',
      'has_title' => '1',
      'title_label' => 'Id Material',
      'custom' => '1',
      'modified' => '1',
      'locked' => '0',
      'disabled' => '0',
      'orig_type' => 'inventario_prendas',
      'disabled_changed' => FALSE,
      'bc_entity_type' => 'node',
    ),
  ),
  'fields' => array(
    'field_grupo_vestu' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_grupo_vestu' => array(
                'value' => 'field_grupo_vestu_value',
                'format' => 'field_grupo_vestu_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_grupo_vestu' => array(
                'value' => 'field_grupo_vestu_value',
                'format' => 'field_grupo_vestu_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_grupo_vestu',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'inventario_prendas',
        ),
      ),
    ),
    'field_imagen_prenda_vestu' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'uri_scheme' => 'private',
        'default_image' => '9119',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_imagen_prenda_vestu' => array(
                'fid' => 'field_imagen_prenda_vestu_fid',
                'alt' => 'field_imagen_prenda_vestu_alt',
                'title' => 'field_imagen_prenda_vestu_title',
                'width' => 'field_imagen_prenda_vestu_width',
                'height' => 'field_imagen_prenda_vestu_height',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_imagen_prenda_vestu' => array(
                'fid' => 'field_imagen_prenda_vestu_fid',
                'alt' => 'field_imagen_prenda_vestu_alt',
                'title' => 'field_imagen_prenda_vestu_title',
                'width' => 'field_imagen_prenda_vestu_width',
                'height' => 'field_imagen_prenda_vestu_height',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'fid' => array(
          'table' => 'file_managed',
          'columns' => array(
            'fid' => 'fid',
          ),
        ),
      ),
      'indexes' => array(
        'fid' => array(
          0 => 'fid',
        ),
      ),
      'field_name' => 'field_imagen_prenda_vestu',
      'type' => 'image',
      'module' => 'image',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'fid' => array(
          'description' => 'The {file_managed}.fid being referenced in this field.',
          'type' => 'int',
          'not null' => FALSE,
          'unsigned' => TRUE,
        ),
        'alt' => array(
          'description' => 'Alternative image text, for the image\'s \'alt\' attribute.',
          'type' => 'varchar',
          'length' => 512,
          'not null' => FALSE,
        ),
        'title' => array(
          'description' => 'Image title text, for the image\'s \'title\' attribute.',
          'type' => 'varchar',
          'length' => 1024,
          'not null' => FALSE,
        ),
        'width' => array(
          'description' => 'The width of the image in pixels.',
          'type' => 'int',
          'unsigned' => TRUE,
        ),
        'height' => array(
          'description' => 'The height of the image in pixels.',
          'type' => 'int',
          'unsigned' => TRUE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'inventario_prendas',
        ),
      ),
    ),
    'field_nombre_prenda_vestu' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_nombre_prenda_vestu' => array(
                'value' => 'field_nombre_prenda_vestu_value',
                'format' => 'field_nombre_prenda_vestu_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_nombre_prenda_vestu' => array(
                'value' => 'field_nombre_prenda_vestu_value',
                'format' => 'field_nombre_prenda_vestu_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_nombre_prenda_vestu',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'inventario_prendas',
        ),
      ),
    ),
    'field_valor_unt_vestu' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_valor_unt_vestu' => array(
                'value' => 'field_valor_unt_vestu_value',
                'format' => 'field_valor_unt_vestu_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_valor_unt_vestu' => array(
                'value' => 'field_valor_unt_vestu_value',
                'format' => 'field_valor_unt_vestu_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_valor_unt_vestu',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'inventario_prendas',
        ),
      ),
    ),
  ),
  'instances' => array(
    'field_grupo_vestu' => array(
      0 => array(
        'label' => 'Grupo',
        'widget' => array(
          'weight' => '-2',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'settings' => array(),
            'module' => 'text',
            'weight' => 2,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_grupo_vestu',
        'entity_type' => 'node',
        'bundle' => 'inventario_prendas',
        'deleted' => '0',
      ),
    ),
    'field_imagen_prenda_vestu' => array(
      0 => array(
        'label' => 'Imagen Prenda',
        'widget' => array(
          'weight' => '-1',
          'type' => 'image_image',
          'module' => 'image',
          'active' => 1,
          'settings' => array(
            'progress_indicator' => 'throbber',
            'preview_image_style' => 'thumbnail',
          ),
        ),
        'settings' => array(
          'file_directory' => '',
          'file_extensions' => 'png gif jpg jpeg',
          'max_filesize' => '',
          'max_resolution' => '',
          'min_resolution' => '',
          'alt_field' => 0,
          'title_field' => 0,
          'default_image' => 0,
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'image',
            'settings' => array(
              'image_style' => '',
              'image_link' => '',
            ),
            'module' => 'image',
            'weight' => 3,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'field_name' => 'field_imagen_prenda_vestu',
        'entity_type' => 'node',
        'bundle' => 'inventario_prendas',
        'deleted' => '0',
      ),
    ),
    'field_nombre_prenda_vestu' => array(
      0 => array(
        'label' => 'Nombre Prenda',
        'widget' => array(
          'weight' => '-4',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '30',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'settings' => array(),
            'module' => 'text',
            'weight' => 0,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_nombre_prenda_vestu',
        'entity_type' => 'node',
        'bundle' => 'inventario_prendas',
        'deleted' => '0',
      ),
    ),
    'field_valor_unt_vestu' => array(
      0 => array(
        'label' => 'Valor UNT',
        'widget' => array(
          'weight' => '-3',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '60',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'settings' => array(),
            'module' => 'text',
            'weight' => 1,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_valor_unt_vestu',
        'entity_type' => 'node',
        'bundle' => 'inventario_prendas',
        'deleted' => '0',
      ),
    ),
  ),
);