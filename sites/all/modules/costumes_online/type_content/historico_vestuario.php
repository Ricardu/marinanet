$data = array(
  'bundles' => array(
    'historico_vestuario' => (object) array(
      'type' => 'historico_vestuario',
      'name' => 'Histórico Vestuario',
      'base' => 'node_content',
      'module' => 'node',
      'description' => '',
      'help' => '',
      'has_title' => '1',
      'title_label' => 'Nodo Solicitud',
      'custom' => '1',
      'modified' => '1',
      'locked' => '0',
      'disabled' => '0',
      'orig_type' => 'historico_vestuario',
      'disabled_changed' => FALSE,
      'bc_entity_type' => 'node',
    ),
  ),
  'fields' => array(
    'field_cantidad_material_histves' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_cantidad_material_histves' => array(
                'value' => 'field_cantidad_material_histves_value',
                'format' => 'field_cantidad_material_histves_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_cantidad_material_histves' => array(
                'value' => 'field_cantidad_material_histves_value',
                'format' => 'field_cantidad_material_histves_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_cantidad_material_histves',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'historico_vestuario',
        ),
      ),
    ),
    'field_id_vestuario_histves' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_id_vestuario_histves' => array(
                'value' => 'field_id_vestuario_histves_value',
                'format' => 'field_id_vestuario_histves_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_id_vestuario_histves' => array(
                'value' => 'field_id_vestuario_histves_value',
                'format' => 'field_id_vestuario_histves_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_id_vestuario_histves',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'historico_vestuario',
        ),
      ),
    ),
    'field_nombre_material_histves' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_nombre_material_histves' => array(
                'value' => 'field_nombre_material_histves_value',
                'format' => 'field_nombre_material_histves_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_nombre_material_histves' => array(
                'value' => 'field_nombre_material_histves_value',
                'format' => 'field_nombre_material_histves_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_nombre_material_histves',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'historico_vestuario',
        ),
      ),
    ),
    'field_talla_material_histves' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_talla_material_histves' => array(
                'value' => 'field_talla_material_histves_value',
                'format' => 'field_talla_material_histves_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_talla_material_histves' => array(
                'value' => 'field_talla_material_histves_value',
                'format' => 'field_talla_material_histves_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_talla_material_histves',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'historico_vestuario',
        ),
      ),
    ),
    'field_unida_reclamar_histves' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'allowed_values' => array(
          0 => array(
            'vocabulary' => 'almacenes_vestuario',
            'parent' => '0',
          ),
        ),
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_unida_reclamar_histves' => array(
                'tid' => 'field_unida_reclamar_histves_tid',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_unida_reclamar_histves' => array(
                'tid' => 'field_unida_reclamar_histves_tid',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'tid' => array(
          'table' => 'taxonomy_term_data',
          'columns' => array(
            'tid' => 'tid',
          ),
        ),
      ),
      'indexes' => array(
        'tid' => array(
          0 => 'tid',
        ),
      ),
      'field_name' => 'field_unida_reclamar_histves',
      'type' => 'taxonomy_term_reference',
      'module' => 'taxonomy',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'tid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'historico_vestuario',
        ),
      ),
    ),
  ),
  'instances' => array(
    'field_cantidad_material_histves' => array(
      0 => array(
        'label' => 'Cantidad Material',
        'widget' => array(
          'weight' => '-2',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '30',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'settings' => array(),
            'module' => 'text',
            'weight' => 2,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_cantidad_material_histves',
        'entity_type' => 'node',
        'bundle' => 'historico_vestuario',
        'deleted' => '0',
      ),
    ),
    'field_id_vestuario_histves' => array(
      0 => array(
        'label' => 'Id Vestuario',
        'widget' => array(
          'weight' => 0,
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '30',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'settings' => array(),
            'module' => 'text',
            'weight' => 4,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_id_vestuario_histves',
        'entity_type' => 'node',
        'bundle' => 'historico_vestuario',
        'deleted' => '0',
      ),
    ),
    'field_nombre_material_histves' => array(
      0 => array(
        'label' => 'Nombre Material',
        'widget' => array(
          'weight' => '-4',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '30',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'settings' => array(),
            'module' => 'text',
            'weight' => 0,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_nombre_material_histves',
        'entity_type' => 'node',
        'bundle' => 'historico_vestuario',
        'deleted' => '0',
      ),
    ),
    'field_talla_material_histves' => array(
      0 => array(
        'label' => 'Talla Material',
        'widget' => array(
          'weight' => '-3',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '30',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'settings' => array(),
            'module' => 'text',
            'weight' => 1,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_talla_material_histves',
        'entity_type' => 'node',
        'bundle' => 'historico_vestuario',
        'deleted' => '0',
      ),
    ),
    'field_unida_reclamar_histves' => array(
      0 => array(
        'label' => 'Unidad Para Reclamar',
        'widget' => array(
          'weight' => '-1',
          'type' => 'options_select',
          'module' => 'options',
          'active' => 1,
          'settings' => array(),
        ),
        'settings' => array(
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'taxonomy_term_reference_link',
            'settings' => array(),
            'module' => 'taxonomy',
            'weight' => 3,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_unida_reclamar_histves',
        'entity_type' => 'node',
        'bundle' => 'historico_vestuario',
        'deleted' => '0',
      ),
    ),
  ),
);