jQuery(document).ready(function(){
	jQuery('#img_up_boton').hide();
	jQuery('.fieldset-title').on( 'click', function() {
		jQuery('#img_up_boton').show();
		jQuery('#tabla_carrito_div').css({display: "none"});
		//se captura el valor del stilo asignado
		var display_mensaje = jQuery('label[for="edit-mensaje-partida"]').css('display');
		//se valida si el mensaje de alerta esta activo
		if(display_mensaje == 'block'){
			//se oculta el mensaje al usuario si el valor de la partida esta bien
        	jQuery('label[for="edit-mensaje-partida"]').hide();
        	//se presnta la imagen del carrito de compra
        	jQuery("#img_carrito_boton").show();
		}
	});
	jQuery('#img_up_boton').on( 'click', function() {
		jQuery("[id*=edit-prendas-inventario-group]").addClass('collapsed');
	 	jQuery("[id*=edit-prendas-inventario-group]").find('.fieldset-wrapper').css({display: "none"});
	 	jQuery('#img_up_boton').hide();
	 	//se captura el valor del stilo asignado
		var display_mensaje = jQuery('label[for="edit-mensaje-partida"]').css('display');
		//se valida si el mensaje de alerta esta activo
		if(display_mensaje == 'block'){
			//se oculta el mensaje al usuario si el valor de la partida esta bien
        	jQuery('label[for="edit-mensaje-partida"]').hide();
        	//se presnta la imagen del carrito de compra
        	jQuery("#img_carrito_boton").show();
		}

	});
	//se valida la funcion de la imagen de carrito de compra
	jQuery('#img_carrito_boton').on('click',function(){
		//se captura el valor del stilo asignado
		var display_div = jQuery('#tabla_carrito_div').css('display');
		//se valida si esta oculto
		if (display_div == 'none') {
			//se cambia el valor para que la tabla sea visible
			jQuery('#tabla_carrito_div').css({display: "block"});
			jQuery("[id*=edit-prendas-inventario-group]").addClass('collapsed');
			jQuery("[id*=edit-prendas-inventario-group]").find('.fieldset-wrapper').css({display: "none"});
	 		jQuery('#img_up_boton').hide();
		}else{
			//en dado caso que se encuentre visible se oculta
			jQuery('#tabla_carrito_div').css({display: "none"});
		}
	});
	jQuery( "#solicitud-vestuario-node-form" ).submit(function( event ) {              
		jQuery('.form-checkbox:checked').each(
		    function() {
		    	//se valida que el valor de el select no sea nulo
				if (jQuery('#edit-field-unidad-para-reclamar-sves-und').val() === "_none"){
					//se captura el id de las prendas seleccionadas
					var id_prendas = jQuery(this).attr('id');
					//se quita de la selección la prenda
					jQuery("#"+id_prendas).prop( "checked", false );
				}
		        //se separa el valor del id del material
	        	var id_valor_material = jQuery(this).val().split('-');
	       		//se captura el id del material seleccionado
	        	var id_material =id_valor_material[0];
	        	jQuery('input[name="campo_cantidad-'+id_material+'"]').prop('disabled', false);
		    }
		);
  		jQuery('#edit-field-valor-deducido-sves-und-0-value').prop('disabled', false);
  		jQuery('#edit-field-valor-disponible-sves-und-0-value').prop('disabled', false);
	});
	jQuery('label[for="edit-mensaje-partida"]').hide();
	//se valida la selección de la tabla de prendas
	jQuery( '.form-checkbox' ).on( 'click', function() {
	    if(jQuery(this).is(':checked')){
	    	//se captura el id del elemento
			var id_check=jQuery(this).attr('id');
	        //se separa el valor del id del material
	        var id_valor_material = jQuery(this).val().split('-');
	        //se captura el id del material seleccionado
	        var id_material =id_valor_material[0];
	        //se captura el valor en limpio de la cantidad
	        var valor_cantidad = jQuery('input[name="campo_cantidad-'+id_material+'"]').val();
	        //se separa el id para capturar el grupo
	        var grupo_prenda = id_check.split('-');
	        //grupo prenda
	        var grupo=grupo_prenda[4].charAt(0).toUpperCase() + grupo_prenda[4].slice(1);
	        //se captura el nombre del valor serleccionado
	        var nombre_inventario = atob(jQuery('input[name="nombre_prenda'+id_material+'"]').val());
	        //se valida el texto del nombre de la prenda
	        var nombreprenda = nombre_inventario.charAt(0).toUpperCase() + nombre_inventario.slice(1);
			if(valor_cantidad == '' ||  valor_cantidad==0){
				alert('Señor usuario el campo cantidad no puede ir en vacio o en 0');
				jQuery("#"+id_check).prop( "checked", false );
			}else{
				//se valida la cantidad del producto
	        	var cantidad= parseFloat(jQuery('input[name="campo_cantidad-'+id_material+'"]').val());
				//se valida el campo en oculti
				jQuery('input[name="campo_cantidad-'+id_material+'"]').prop('disabled', true);
		        //se captura el valor de la prenda
		        var valor_unt = id_valor_material[1].replace(/,/g,'');
		        //se quita el decimal
		       // var unit_deci = valor_unt.substring(0,valor_unt.length-3);
		        //se multiplica la cantidad por el precio unitario del producto
		        var total_producto = valor_unt * cantidad;
		        //se define la variable que se suma al final
		        var varlorfinal_deducido="";
		        //se captura el valo inicial del deducido
		        var valor_principal_deducido=jQuery('#edit-field-valor-deducido-sves-und-0-value').val();
		        //se valida si el valor final del deducido es 0
		        if (valor_principal_deducido  === '0') {
		        	//se convierte en float para poderlo sumar
		        	varlorfinal_deducido = parseFloat(valor_principal_deducido);
		        }else{
		        	//se quita el primer caracter del deducido
			       	var valor_deducido = valor_principal_deducido.substring(1);
			        //se reemplazan las comas en el valor
			        var valor_deducido_coma = valor_deducido.replace(/,/g,'');
			        //se quitan los centavos
			      	//var deducido= valor_deducido_coma.substring(0,valor_deducido_coma.length-3).toString();
			        //se convierte el valor a float
		        	varlorfinal_deducido = parseFloat(valor_deducido_coma);
		        }
		        //se realiza la suma del valor deducido mas el actual
		       	var total_deducido =total_producto + varlorfinal_deducido;
		        //se asigna el total deducido
		        jQuery("label[for='edit-floatpartidad-deducido']").html('Total Deducido: $' + parseFloat(total_deducido, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
		        jQuery('#edit-field-valor-deducido-sves-und-0-value').val('$' + parseFloat(total_deducido, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
		        jQuery('#edit-field-valor-deducido-sves-und-0-value').attr('value',jQuery('#edit-field-valor-deducido-sves-und-0-value').val());
		        jQuery('#edit-field-valor-deducido-sves-und-0-value').attr('value',jQuery('#edit-field-valor-deducido-sves-und-0-value').val());
		        //se captura el valor del total disponible en el momento
		        var val_disponible = jQuery('#edit-field-valor-disponible-sves-und-0-value').val();
		        //se quita el primer caracter del deducido
		       	var val_disponible1 = val_disponible.substring(1);
		        //se reemplazan las comas en el valor
		        var val_disponible12 = val_disponible1.replace(/,/g,'');
		        val_disponible123 = parseFloat(val_disponible12);
		        //se realiza la función que permite quitar el deducido al disponible
		        var total_disponible = val_disponible123 - total_producto;
	 			jQuery('#edit-field-valor-disponible-sves-und-0-value').val('$' + parseFloat(total_disponible, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
		       	jQuery('#edit-field-valor-disponible-sves-und-0-value').attr('value',jQuery('#edit-field-valor-disponible-sves-und-0-value').val());
		       	jQuery('#edit-field-valor-disponible-sves-und-0-value').attr('value',jQuery('#edit-field-valor-disponible-sves-und-0-value').val());
		       	jQuery('#tabla_carrito tr:last').after('<tr class="odd" id = "'+id_material+'"><td>'+grupo+'</td><td>'+nombreprenda+'</td><td style="text-align: center;">'+valor_cantidad+'</td><td> $'+parseFloat(total_producto, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</td></tr>');
		       	//se valida que el valor no se pase del presupuesto
		        if(total_disponible < 0){
		        	//se realiza la suma del valor deducido mas el actual
			       	var total_deducido =total_deducido-total_producto;
			        //se asigna el total deducido
			        jQuery("label[for='edit-floatpartidad-deducido']").html('Total Deducido: $' + parseFloat(total_deducido, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
			        jQuery('#edit-field-valor-deducido-sves-und-0-value').val('$' + parseFloat(total_deducido, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
			       	jQuery('#edit-field-valor-deducido-sves-und-0-value').attr('value',jQuery('#edit-field-valor-deducido-sves-und-0-value').val());
			       	jQuery('#edit-field-valor-deducido-sves-und-0-value').attr('value',jQuery('#edit-field-valor-deducido-sves-und-0-value').val());
			        //se realiza la función que permite quitar el deducido al disponible
			        var total_disponible = total_disponible + total_producto;
		 			//se quita el punto del valor
		 			var valor_id_unit = valor_unt.replace('.','');
		 			//se elimina el checked al campo correspondiente
			        jQuery("#"+id_check).prop( "checked", false );
			        //se activa el campo de cantidad
			        jQuery('input[name="campo_cantidad-'+id_material+'"]').prop('disabled', false);
			        //se prensenta el valor del campo total disponible
		 			jQuery('#edit-field-valor-disponible-sves-und-0-value').val('$' + parseFloat(total_disponible, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
		        	jQuery('#edit-field-valor-disponible-sves-und-0-value').attr('value',jQuery('#edit-field-valor-disponible-sves-und-0-value').val());
		        	jQuery('#edit-field-valor-disponible-sves-und-0-value').attr('value',jQuery('#edit-field-valor-disponible-sves-und-0-value').val());
		        	//se realiza el seteo del mensaje que se presenta al usuario cuando el valor supera la partida
		        	jQuery('label[for="edit-mensaje-partida"]').html('Señor usuario la prenda que intenta registrar supera el valor disponible de su partidad por favor verifique la cantidad o seleccione otra prenda. Gracias');
		        	//se muestra el mensaje al usuario
		        	jQuery('label[for="edit-mensaje-partida"]').show();
		        	//funcion que permite mostrar el mensaje en la parte superior de la pagina
		        	jQuery("html, body").animate({ scrollTop: 0 }, 600);
		        	//se oculta la imagen del carrito de compra
		        	jQuery("#img_carrito_boton").hide();
		        	jQuery('#tabla_carrito').find('#'+id_material).remove();
		        }else{
		        	//se oculta el mensaje al usuario si el valor de la partida esta bien
		        	jQuery('label[for="edit-mensaje-partida"]').hide();
		        	//se presnta la imagen del carrito de compra
		        	jQuery("#img_carrito_boton").show();
		        }
			}   
	    } else {
        	//se separa el valor del id del material
	        var id_valor_material = jQuery(this).val().split('-');
	        //se captura el id del material seleccionado
	        var id_material =id_valor_material[0];
	        //se valida la cantidad del producto
	        var cantidad= parseFloat(jQuery('input[name="campo_cantidad-'+id_material+'"]').val());
	        jQuery('input[name="campo_cantidad-'+id_material+'"]').prop('disabled', false);
	        //se captura el valor de la prenda
	        var valor_unt = id_valor_material[1].replace(/,/g,'');
	        //se quita el decimal
	       	//var unit_deci = valor_unt.substring(0,valor_unt.length-3);
	        //se multiplica la cantidad por el precio unitario del producto
	        var total_producto = valor_unt * cantidad;
	        //se define la variable que se suma al final
	        var varlorfinal_deducido="";
	        //se captura el valo inicial del deducido
	        var valor_principal_deducido=jQuery('#edit-field-valor-deducido-sves-und-0-value').val();
	        //se valida si el valor final del deducido es 0
	        if (valor_principal_deducido  === '0.00') {
	        	//se convierte en float para poderlo sumar
	        	varlorfinal_deducido = parseFloat(valor_principal_deducido);
	        }else{
	        	//se quita el primer caracter del deducido
		       	var valor_deducido = valor_principal_deducido.substring(1);
		        //se reemplazan las comas en el valor
		        var valor_deducido_coma = valor_deducido.replace(/,/g,'');
		        //se quitan los centavos
		     	//var deducido= valor_deducido_coma.substring(0,valor_deducido_coma.length-3).toString();
		        //se convierte el valor a float
	        	varlorfinal_deducido = parseFloat(valor_deducido_coma);
	        }
	        //se realiza la suma del valor deducido mas el actual
	        var total_deducido = varlorfinal_deducido - total_producto;
	        //se asigna el total deducido
	        jQuery("label[for='edit-floatpartidad-deducido']").html('Total Deducido: $' + parseFloat(total_deducido, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
	        jQuery('#edit-field-valor-deducido-sves-und-0-value').val('$' + parseFloat(total_deducido, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
	        jQuery('#edit-field-valor-deducido-sves-und-0-value').attr('value',jQuery('#edit-field-valor-deducido-sves-und-0-value').val());
	        jQuery('#edit-field-valor-deducido-sves-und-0-value').attr('value',jQuery('#edit-field-valor-deducido-sves-und-0-value').val());
	        //se asigna el total deducido
	        //se captura el valor del total disponible en el momento
        	var val_disponible = jQuery('#edit-field-valor-disponible-sves-und-0-value').val();
	        //se quita el primer caracter del deducido
	       	var val_disponible1 = val_disponible.substring(1);
	        //se reemplazan las comas en el valor
	        var val_disponible12 = val_disponible1.replace(/,/g,'');
	        val_disponible123 = parseFloat(val_disponible12);
	        //se realiza la función que permite quitar el deducido al disponible
	        var total_disponible = val_disponible123 + total_producto;
 			jQuery('#edit-field-valor-disponible-sves-und-0-value').val('$' + parseFloat(total_disponible, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
	    	jQuery('#edit-field-valor-disponible-sves-und-0-value').attr('value',jQuery('#edit-field-valor-disponible-sves-und-0-value').val());
	    	jQuery('#edit-field-valor-disponible-sves-und-0-value').attr('value',jQuery('#edit-field-valor-disponible-sves-und-0-value').val());
	    	jQuery('#tabla_carrito').find('#'+id_material).remove();
	    }
	});
});