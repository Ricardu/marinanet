/**
	Archivo que valida el formulario de edición de vestuario en linea
*/
jQuery(document).ready(function(){
	var id_element = Drupal.settings.costumes_online.id_element; 
	//alert(id_element);
	jQuery.each( id_element, function( index, value ){
		jQuery("#"+index).prop( "checked", true );
		//se separa el valor del id del material
		var id_valor_material = index.split('-');
		//se captura el id del material seleccionado
		var id_material =id_valor_material[7];
		//se separa el valor de la talla con el de la cantidad
		var cantidad_talla = value.split('-');
		//se asigna el valor de la cantidad registrado en el historico
		jQuery('input[name="campo_cantidad-'+id_material+'"]').val(cantidad_talla[1]);
		//se valida que no se pueda editar el formulario
		jQuery('input[name="campo_cantidad-'+id_material+'"]').prop('disabled', true);
		//se asigna el valor de la talla registrado en el historico
		jQuery('input[name="campo_talla-'+id_material+'"]').val(cantidad_talla[0]);
		//se captura el valor del grupo
		var grupo_inventario = id_valor_material[4].charAt(0).toUpperCase() + id_valor_material[4].slice(1);
		//se captura el valor de la prenda
		var valor_prenda = id_valor_material[8];
		//se captura le valor del total de la cantidad
		var valor_cantidad_tabla = cantidad_talla[1];
		//se calcula el valor total
		var total_producto_tabla = valor_prenda * valor_cantidad_tabla;
 		//se captura el nombre del valor serleccionado
        var nombre_inventario = atob(jQuery('input[name="nombre_prenda'+id_material+'"]').val());
        //se valida el texto del nombre de la prenda
        var nombreprenda = nombre_inventario.charAt(0).toUpperCase() + nombre_inventario.slice(1);
		//se asigna en la tabla
		jQuery('#tabla_carrito tr:last').after('<tr class="odd" id = "'+id_material+'"><td>'+grupo_inventario+'</td><td>'+nombreprenda+'</td><td style="text-align: center;">'+valor_cantidad_tabla+'</td><td> $'+parseFloat(total_producto_tabla, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</td></tr>');
	});
});