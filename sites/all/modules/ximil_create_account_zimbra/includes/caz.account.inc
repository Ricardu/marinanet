<?php
/* funcion para probar que la api del zimbra este bien configurada y haya comunicación con el servidor */
function caz_test(){
	// Making sure the Zimbra library can be found
	$zimbra_api_library_path = libraries_get_path('api-php-zimbra');
	require_once $zimbra_api_library_path.'/SplClassLoader.php'; // The PSR-0 autoloader from https://gist.github.com/221634
	$path_zimbra_api=drupal_realpath($zimbra_api_library_path.'/src');
	$classLoader = new SplClassLoader('Zimbra', $path_zimbra_api); // Point this to the src folder of the zcs-php repo
	$classLoader->register();
	// Define some constants we're going to use
	// Create a new Admin class and authenticate
	$zimbra = new \Zimbra\ZCS\Admin(ZIMBRA_SERVER, ZIMBRA_PORT);
	$zimbra->auth(ZIMBRA_LOGIN, ZIMBRA_PASS);
	// Get all available accounts from a domain
	// And output them
	$html="";
	$result=$zimbra->addDistributionListMember('b019f95a-1bff-4bd5-8bf4-7e5f7b321682','fernando.freire.gomez2@armada.mil.co');
	//$zimbra->modifyDistributionList($params);

	//caz_create_account_ldap();



	//$ds = ldap_connect(LDAP_SERVER,LDAP_PORT);  // Asumiendo que el servidor de LDAP está en el mismo host
	//ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
	//if ($ds) {
	    // Asociar con el dn apropiado para dar acceso de actualización
	  //  $r = ldap_bind($ds, LDAP_LOGIN,LDAP_PASS);

	    // Preparar los datos

	    //$info['dn']='uid=pruebas.freire,ou=armada.mil.co,o=armada,c=co';
	    $info['objectClass'][0]='qmailUser';
	    $info['objectClass'][1]='posixAccount';
	    $info['objectClass'][2]='sambaSamAccount';
	    $info['objectClass'][3]='shadowAccount';
	    $info['objectClass'][4]='inetOrgPerson';
	    $info['qmailUID']='1';
	    $info['qmailGID']='0';
	    $info['mailMessageStore']='/home/vpopmail/domains/armada.mil.co/e/pruebas.freire/';
	    $info['mail']='pruebas.freire@armada.mil.co';
	    $info['sambaDomainName']='MEDUSA';
	    $info['sambaHomeDrive']='U';
	    $info['sambaKickoffTime']='1893474000';
	    $info['sambaAcctFlags']='[XU         ]';
	    $info['displayName']='Esmeralda Alarcon';
	    $info['shadowWarning']='10';
	    $info['shadowInactive']='10';
	    $info['shadowMin']='1';
	    $info['shadowMax']='365';
	    $info['shadowExpire']='21915';
	    $info['homeDirectory']='/home/pruebas.freire';
	    $info['loginShell']='/bin/false';
	    $info['gidNumber']='10000';
	    $info['uid']='pruebas.freire';
	    $info['uidNumber']='10487';
	    $info['sambaPrimaryGroupSID']='S-1-5-21-4018220718-1252062224-979654447-512';
	    $info['shadowLastChange']='14166';
	    $info['mailQuota']='50000000';
	    $info['postOfficeBox']='Bogota';
	    $info['sambaSID']='S-1-5-21-4018220718-1252062224-979654447-512';
	    $info['sambaPwdLastSet']='1270836116';
	    $info['mobile']='3144674781';
	    $info['clearPassword']='76225ced7cbc9b51fcf4a5765d11b52f71d67339';
	    $info['cn']='Jeyme Esmeralda Alarcon Barragan';
	    $info['qmaildomain']='Jeyme Esmeralda Alarcon Barragan';
	    $info['sambaLMPassword']='9871D0BE0DCF32399165AD65FCC688BB';
	    $info['sambaNTPassword']='F43F0AE079AA572C10B361EEAEA62256';
	    $info['userPassword']='{MD5}GbHCwkTgaju05e+3hOaeVw==';
	    $info['employeeType']='CIVIL';
	    $info['postalCode']='52326308';
	    $info['homePhone']='3692000 Ext 10163';
	    $info['givenName']='Jeymy Esmeralda';
	    $info['sn']='Alarcon Barragan';
	    $info['description']='Administradora de Correo Institucional';
	    $info['departmentNumber']='JOLA';
	    $info['homePostalAddress']='9b49bbfa0909eb61cee386716f5100b9b857776c';
	    $info['employeeNumber']='1';

	    // Agregar datos al directorio
/*	    if(ldap_add($ds, "uid=pruebas.freire,ou=armada.mil.co,o=armada,c=co", $info)){
            echo "agregado";
	    }
	    else{
	    	  echo ldap_error($ds);
	    	  echo "fallo insert";
	    }


	    ldap_close($ds);
	} else {
	    echo "No se pudo conectar al servidor LDAP";
	}
	exit();*/	
        //$node=node_load(3301);
        //caz_create_account_ldap($node);

	/*	$params=array('name'=>'ffreire@armada.mil.co',
	 'givenName'=>'Fernando',
			'initials'=>'A',
			'sn'=>'freire',
			'password'=>'123456');*/
	//	$reponse=$zimbra->createAccount($params);

	//   $zimbra->modifyAccount(array('id'=>'8566a072-ac00-4614-8df9-fc93e260658a','zimbraMailTransport'=>'lmtp:armada.mil.co:7025','zimbraMailHost'=>'armada.mil.co'))*/
	return "<span>test<span>";
}
/**/
function caz_asigned_mail($node){
	// cargarmos el primer nombre
	$primer_nombre=$node->field_primer_nombre_caz['und'][0]['value'];
	// cargarmos el segundo nombre
	$segundo_nombre=isset($node->field_segundo_nombre_caz['und'][0]['value'])?$node->field_segundo_nombre_caz['und'][0]['value']:'';
	// cargamos el primer apellido
	$primer_apellido=$node->field_primer_apellido_caz['und'][0]['value'];
	// cargamos el segundo apellido si fue digitado
	$segundo_apellido=isset($node->field_segundo_apellido_caz['und'][0]['value'])?$node->field_segundo_apellido_caz['und'][0]['value']:'';
	// se define el dominio
	$domain_mail='@armada.mil.co';
	// se contena la antes del @ del correo
	$account=caz_limpiar_cadena($primer_nombre.'.'.$primer_apellido);
	// se define el correo en formato primernombre.primerapellido@domino
	$mail=$account.$domain_mail;
	// se define la bandera para seguir buscando
	$flag_continua=true;
	// si no existe la cuenta de correo quiere decir que esta disponible para ser creada y se coloca la bande en falso para que no siga buscando
	if(!search_account_by_mail($mail)){
		$flag_continua=false;
	}
	// si la bandera esta prendida y el segundo apellido no es vacio  se arma el correo con el formato primernombre.primerapellido.segundoapellido@domino
	if($flag_continua && !empty($segundo_apellido)){
		// se obtine el largo del apellido
		$largo_segundo_apellido=strlen($segundo_apellido);
		// se inicializa el contador del caracter del apellido a concatenar
		$contador=0;
		// se concatena el name con un punto
		$account.='.';
		// se inicializa la bandera de seguir concatenando letra a letra del apellido con el nombre de usuario
		$flag_concatena_apellido=true;
		while($flag_concatena_apellido){
			  // se adiciona la letra del apellido a el nombre de usuario
		      $account.=substr(caz_limpiar_cadena($segundo_apellido),$contador,1);
		      // se arma el correo para ver si existe en el servidor zimbra
			  $mail=$account.$domain_mail;
			  // se verifica si el correo esta disponible para usar y se modifican la banderas para terminar la busqueda
			  if(!search_account_by_mail($mail)){
				  $flag_concatena_apellido=false;
				  $flag_continua=false;
			  }
			  $contador++;
			  if ($contador >= $largo_segundo_apellido){
                  $flag_concatena_apellido=false;
			  }
		}
		
	}
	// si la bandera esta prendida se adiciona un número al final hasta encontrar un correo disponible para crear
	if ($flag_continua){
		// inicializa el contador en 1
		$contador=1;
		while($flag_continua){
			$mail=$account.$contador.$domain_mail;
			if(!search_account_by_mail($mail)){
				$flag_continua=false;
			}
			$contador++;
		}
	}

	return $mail;
}
/* varible que consulta si existe el buzon en el zimbra */
function search_account_by_mail($mail=''){
	$flag_encontro=false;
	if (!empty($mail)){
		$zimbra_api_library_path = libraries_get_path('api-php-zimbra');
		require_once $zimbra_api_library_path.'/SplClassLoader.php'; // The PSR-0 autoloader from https://gist.github.com/221634
		$path_zimbra_api=drupal_realpath($zimbra_api_library_path.'/src');
		$classLoader = new SplClassLoader('Zimbra', $path_zimbra_api); // Point this to the src folder of the zcs-php repo
		$classLoader->register();
		// Create a new Admin class and authenticate
		$zimbra = new \Zimbra\ZCS\Admin(ZIMBRA_SERVER, ZIMBRA_PORT);
		$zimbra->auth(ZIMBRA_LOGIN, ZIMBRA_PASS);
		try {
			$result = $zimbra->getAccount('armada.mil.co','name',$mail);
			if($result->name==$mail){
				$flag_encontro=true;
			}
		} catch (Exception $e) {
			$flag_encontro=false;
		}

	}
	return $flag_encontro;
}
function caz_create_account_zimbra($node){
	$zimbra_api_library_path = libraries_get_path('api-php-zimbra');
	require_once $zimbra_api_library_path.'/SplClassLoader.php'; // The PSR-0 autoloader from https://gist.github.com/221634
	$path_zimbra_api=drupal_realpath($zimbra_api_library_path.'/src');
	$classLoader = new SplClassLoader('Zimbra', $path_zimbra_api); // Point this to the src folder of the zcs-php repo
	$classLoader->register();
	$zimbra = new \Zimbra\ZCS\Admin(ZIMBRA_SERVER, ZIMBRA_PORT);
	$zimbra->auth(ZIMBRA_LOGIN, ZIMBRA_PASS);
	$name=isset($node->field_correo_caz['und'][0]['value'])?trim($node->field_correo_caz['und'][0]['value']):'';
	$primer_nombre=trim($node->field_primer_nombre_caz['und'][0]['value']);
	$segundo_nombre=isset($node->field_segundo_nombre_caz['und'][0]['value'])?trim($node->field_segundo_nombre_caz['und'][0]['value']):'';
	$segundo_nombre=caz_limpiar_cadena($segundo_nombre);
	$primer_apellido=caz_limpiar_cadena($node->field_primer_apellido_caz['und'][0]['value']);
	$segundo_apellido=isset($node->field_segundo_apellido_caz['und'][0]['value'])?trim($node->field_segundo_apellido_caz['und'][0]['value']):'';
	$segundo_apellido=caz_limpiar_cadena($segundo_apellido);
	$password=isset($node->field_password_caz['und'][0]['pass1'])?trim($node->field_password_caz['und'][0]['pass1']):'';
	$primer_nombre_str=caz_limpiar_cadena($primer_nombre);
	$flag_creacion=false;
	if(!empty($name)){
		$params=array('name'=>$name,
				      'givenName'=>$primer_nombre_str,
				      'initials'=>(!empty($segundo_nombre))?substr($segundo_nombre, 1,1):'',
				      'sn'=>$primer_apellido.' '.$segundo_apellido,
				      'cn'=>$primer_apellido.' '.$segundo_apellido,
				      'displayName'=>$primer_nombre_str.' '.$segundo_nombre.' '.$primer_apellido.' '.$segundo_apellido,
				      'password'=>$password);
		try {
			$result = $zimbra->createAccount($params);
			if($result->name==$name){
				$flag_creacion=true;
				$zimbra->addDistributionListMember(ZIMBRA_LIST_DISTRIBUTION,$name);
				$server_destination=taxonomy_term_load($node->field_unidad_caz['und'][0]['tid']);
				if (isset($server_destination->field_zimbramailhost_caz['und'][0]['value'])){
					$zimbra->modifyAccount(array('id'=>$result->id,'zimbraMailHost'=>$server_destination->field_zimbramailhost_caz['und'][0]['value']));
				}
				if (isset($server_destination->	field_zimbramailtransport_caz['und'][0]['value'])){
					$zimbra->modifyAccount(array('id'=>$result->id,'zimbraMailTransport'=>$server_destination->	field_zimbramailtransport_caz['und'][0]['value']));
				}
                                $host_ldap=variable_get('caz_user_server_ldap','');
                                if(!empty($host_ldap)){
				   $flag_creacion=caz_create_account_ldap($node);
                                }
			}
		} catch (Exception $e) {
			drupal_set_message('Error al crear cuenta de correo','error');
			$flag_creacion=false;
		}
	}
	return $flag_creacion;

}
/**
Diego Giraldo ARC Permite conectar y validar si el usuario existe en el ldap
*/
function caz_conect_ldap($identification){
	$ds = ldap_connect(LDAP_SERVER,LDAP_PORT);  // Asumiendo que el servidor de LDAP está en el mismo host
	ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
	if ($ds) {
		// Asociar con el dn apropiado para dar acceso de actualización
		$r = ldap_bind($ds, LDAP_LOGIN, LDAP_PASS);
        if (!$r){
	       drupal_set_message('Error al intentar acceder como administrador del ldap '.LDAP_SERVER.' '.LDAP_PORT.' '.LDAP_LOGIN.' '.LDAP_PASS);
        }else{
    		$search = ldap_search($ds, LDAP_FILTRO, 'postalCode'.'='.$identification);
    		$data = ldap_get_entries($ds, $search);
    		if(!empty($data["count"])){
	    		for ($i=0; $i<$data["count"]; $i++) {
	            	if(isset($data[$i]["uid"][0])) {
	             	   $uid_user=$data[$i]["uid"][0];
	             	   drupal_set_message('El usuario ya existe en el ldap '.$uid_user);
	           		}
        		}
    		}
    		else{
       			drupal_goto('/node/add/cuenta-correo-institucional/'.$identification);
	        }
        }  
	}
}
function caz_create_account_ldap($node){
	$flag_create=false;
	$ds = ldap_connect(LDAP_SERVER,LDAP_PORT);  // Asumiendo que el servidor de LDAP está en el mismo host
	ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
	if ($ds) {
		// Asociar con el dn apropiado para dar acceso de actualización
		$r = ldap_bind($ds, LDAP_LOGIN, LDAP_PASS);
                if (!$r){
                   drupal_set_message('Error al intentar acceder como administrador del ldap '.LDAP_SERVER.' '.LDAP_PORT.' '.LDAP_LOGIN.' '.LDAP_PASS);
                }  
		$correo=isset($node->field_correo_caz['und'][0]['value'])?$node->field_correo_caz['und'][0]['value']:'';
		//$correo="fernando.freire@armada.mil.co";
		$primer_nombre=caz_limpiar_cadena($node->field_primer_nombre_caz['und'][0]['value']);
		$segundo_nombre=isset($node->field_segundo_nombre_caz['und'][0]['value'])?caz_limpiar_cadena($node->field_segundo_nombre_caz['und'][0]['value']):'';
		$segundo_nombre=caz_limpiar_cadena($segundo_nombre);
		$primer_apellido=caz_limpiar_cadena($node->field_primer_apellido_caz['und'][0]['value']);
		$segundo_apellido=isset($node->field_segundo_apellido_caz['und'][0]['value'])?caz_limpiar_cadena($node->field_segundo_apellido_caz['und'][0]['value']):'';
		$segundo_apellido=caz_limpiar_cadena($segundo_apellido);
		$password=isset($node->field_password_caz['und'][0]['pass1'])?$node->field_password_caz['und'][0]['pass1']:'';
		$identification=$node->field_caz_numero_cedula['und'][0]['value'];
		$cn=explode('@', $correo);
		$dn="uid=".$cn[0].",".LDAP_FILTRO;
        $info=array(); 
	    $info['objectClass'][0]='qmailUser';
	    $info['objectClass'][1]='posixAccount';
	    $info['objectClass'][2]='sambaSamAccount';
	    $info['objectClass'][3]='shadowAccount';
	    $info['objectClass'][4]='inetOrgPerson';
	    $info['qmailUID']='1';
	    $info['qmailGID']='0';
	    $info['mailMessageStore']='/home/vpopmail/domains/armada.mil.co/e/'.$cn[0].'/';
	    $info['mail']=$correo;
	    $info['sambaDomainName']='MEDUSA';
	    $info['sambaHomeDrive']='U';
	    $info['sambaKickoffTime']='1893474000';
	    $info['sambaAcctFlags']='[XU         ]';
	    $info['displayName']=$primer_nombre.' '.$primer_apellido;
	    $info['shadowWarning']='10';
	    $info['shadowInactive']='10';
	    $info['shadowMin']='1';
	    $info['shadowMax']='365';
	    $info['shadowExpire']='21915';
	    $info['homeDirectory']='/home/'.$cn[0];
	    $info['loginShell']='/bin/false';
	    $info['gidNumber']='10000';
	    $info['postalCode']=$identification;
	    $info['uid']=$cn[0];
	    $uid=variable_get('caz_ldap_uidnumber',10488) + 1;
	    variable_set('caz_ldap_uidnumber',$uid);
	    $info['uidNumber']=$uid;
	    $info['sambaPrimaryGroupSID']=LDAP_GROUP_SAMBA;
	    $info['shadowLastChange']='14166';
	    $info['mailQuota']='50000000';
	    //$info['postOfficeBox']='';
	    $info['sambaSID']=LDAP_SID_SAMBA;
	    $info['sambaPwdLastSet']='1270836116';
	    //$info['mobile']='';
	    //$info['clearPassword']='';
	    $info['cn']=$primer_nombre.' '.$segundo_nombre.' '.$primer_apellido.' '.$segundo_apellido;
	    $info['qmaildomain']=$primer_nombre.' '.$segundo_nombre.' '.$primer_apellido.' '.$segundo_apellido;
	    $info['sambaLMPassword']='9871D0BE0DCF32399165AD65FCC688BB';
	    $info['sambaNTPassword']='F43F0AE079AA572C10B361EEAEA62256';
	    $info['userPassword']='{SHA}'.$password;
	    //$info['employeeType']='';
	    //$info['postalCode']='';
	    //$info['homePhone']='';
	    $info['givenName']=$primer_nombre.' '.$segundo_nombre;
	    $info['sn']=$primer_apellido.' '.$segundo_apellido;
	    //$info['description']='';
	    //$info['departmentNumber']='';
	    //$info['homePostalAddress']='';
	    $info['employeeNumber']='1';
	    if(ldap_add($ds, $dn, $info)){
            $flag_create=true;
	    }
	    else{
	    	  drupal_set_message('Error al crear usuario en el ldap '.ldap_error($ds).' '.$dn,'error');
			  echo "<pre>";
			  print_r($info);
			  echo "</pre>";
	    }
		ldap_close($ds);
	}
	return $flag_create;
}

function caz_limpiar_cadena($string)
{
	$string = trim(strtr(strtolower($string),"ÀÈÌÒÙÁÉÍÓÚÇÑÄËÏÖÜ","àèìòùáéíóúçñäëïöü"));
	$string = str_replace(
			array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
			array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
			$string
	);

	$string = str_replace(
			array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
			array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
			$string
	);

	$string = str_replace(
			array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
			array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
			$string
	);

	$string = str_replace(
			array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
			array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
			$string
	);

	$string = str_replace(
			array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
			array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
			$string
	);

	$string = str_replace(
			array('ñ', 'Ñ', 'ç', 'Ç'),
			array('n', 'N', 'c', 'C',),
			$string
	);

	//Esta parte se encarga de eliminar cualquier caracter extraño
	$string = str_replace(
			array("\\", "¨", "º", "-", "~",
					"#", "@", "|", "!", "\"",
					"·", "$", "%", "&", "/",
					"(", ")", "?", "'", "¡",
					"¿", "[", "^", "`", "]",
					"+", "}", "{", "¨", "´",
					">", "< ", ";", ",", ":",
					" "),
			'',
			$string
	);


	return $string;
}
