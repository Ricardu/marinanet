<?php
/** funcion que realiza mantenimiento a las variables de configuracion del modulo
 * @retun array con los elementos de formulario (utiliza api form);
 */
function  caz_admin_page(){
    $form=array(); // se inicializa el arreglo que almacenera los elementos del fomulario	
    
	$form['caz_host_server_ldap'] = array(
			'#type' => 'textfield',
			'#title' => t('host servidor ldap'),
			'#description' => t(''),
			'#default_value' => variable_get('caz_host_server_ldap',''),
	);
	$form['caz_port_server_ldap'] = array(
			'#type' => 'textfield',
			'#title' => t('puerto servidor ldap'),
			'#description' => t(''),
			'#default_value' => variable_get('caz_port_server_ldap',''),
	);
	$form['caz_user_server_ldap'] = array(
			'#type' => 'textfield',
			'#title' => t('usuario admin ldap'),
			'#description' => t(''),
			'#default_value' => variable_get('caz_user_server_ldap',''),
	);
	$form['caz_password_server_ldap'] = array(
			'#type' => 'password',
			'#title' => t('password usuario admin ldap'),
			'#description' => t(''),
			'#default_value' => variable_get('caz_password_server_ldap',''),
	);
	$form['caz_ldap_filter'] = array(
			'#type' => 'textfield',
			'#title' => t('filter of search'),
			'#description' => t(''),
			'#default_value' => variable_get('caz_ldap_filter',''),
	);
	$form['caz_ldap_gidnumber'] = array(
			'#type' => 'textfield',
			'#title' => t('Gid Number'),
			'#description' => t(''),
			'#default_value' => variable_get('caz_ldap_gidnumber',''),
	);
	$form['caz_ldap_uidnumber'] = array(
			'#type' => 'textfield',
			'#title' => t('Uid Number last'),
			'#description' => t(''),
			'#default_value' => variable_get('caz_ldap_uidnumber',''),
	);
	
	$form['caz_host_server_zimbra'] = array(
			'#type' => 'textfield',
			'#title' => t('host servidor principal zimbra'),
			'#description' => t(''),
			'#default_value' => variable_get('caz_host_server_zimbra',''),
	);
	$form['caz_port_server_zimbra'] = array(
			'#type' => 'textfield',
			'#title' => t('puerto servidor zimbra'),
			'#description' => t(''),
			'#default_value' => variable_get('caz_port_server_zimbra',''),
	);
	
	$form['caz_user_admin_zimbra'] = array(
			'#type' => 'textfield',
			'#title' => t('usuario admin zimbra'),
			'#description' => t(''),
			'#default_value' => variable_get('caz_user_admin_zimbra',''),
	);
	$form['caz_zimbra_list_distribution'] = array(
			'#type' => 'textfield',
			'#title' => t('Id lista de distribución'),
			'#description' => t(''),
			'#default_value' => variable_get('caz_zimbra_list_distribution',''),
	);
	$form['caz_user_group_samba'] = array(
			'#type' => 'textfield',
			'#title' => t('grupo samba'),
			'#description' => t(''),
			'#default_value' => variable_get('caz_user_group_samba',''),
	);
	$form['caz_sid_samba'] = array(
			'#type' => 'textfield',
			'#title' => t('sid samba'),
			'#description' => t(''),
			'#default_value' => variable_get('caz_sid_samba',''),
	);
	$form['caz_password_admin_zimbra'] = array(
			'#type' => 'password',
			'#title' => t('password usuario admin zimbra'),
			'#description' => t(''),
			'#default_value' => variable_get('caz_password_admin_zimbra',''),
	);
	//campo en el cual se almacena la url para consultar el siath
	$form['caz_siath_ws_url'] = array(
			'#type' => 'textfield',
			'#title' => t('URL del webservice de consulta de datos en el SIATH.'),
			'#default_value' => variable_get('caz_siath_ws_url', 'http://www.armada.mil.co/arc_ws_siath/siathWS.wsdl'),
			'#description' => t("URL de publicación del webservice de consulta de información en el SIATH"),
			'#required' => TRUE,
	);
	//key del webservice
	$form['caz_siath_ws_key'] = array(
			'#type' => 'textfield',
			'#title' => t('Llave de acceso al webservice.'),
			'#default_value' => variable_get('caz_siath_ws_key', 'alksdjfwoieur'),
			'#description' => t("Llave del webservice de consulta de información en el SIATH"),
			'#required' => TRUE,
	);
	//campo que almacena la cantidad de preguntas que se le presentan al usuario
	$form['caz_questions_quantity'] = array(
			'#type' => 'textfield',
			'#title' => t('Cantidad de preguntas.'),
			'#default_value' => variable_get('caz_questions_quantity', '3'),
			'#description' => t("Cantidad de preguntas que se realizarán para la validación de identidad."),
			'#required' => TRUE,
	);
	//Campo  que almacena la cantidad de respuestas
	$form['caz_answers_quantity'] = array(
			'#type' => 'textfield',
			'#title' => t('Cantidad de respuestas.'),
			'#default_value' => variable_get('caz_answers_quantity', '3'),
			'#description' => t("Cantidad de respuestas que se incluiran para cada pregunta de validación"),
			'#required' => TRUE,
	);
	return system_settings_form($form);
}