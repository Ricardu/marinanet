//funcion que permite ejecutar directamente las lineas jQuery
jQuery(document).ready(function(){
	//se valida al momento de digitar en el campo
	jQuery('#edit-field-telefono-rutav-und-0-value').keyup(function (){
 		//se valida que solo acepte números con una expresión regular
 		this.value = (this.value + '').replace(/[^0-9^+]/g, '');
	});
	//se valida el campo de confirmación de celular
	jQuery('#edit-field-telefono2-rutav-und-0-value').keyup(function (){
 		//se valida que solo acepte números con una expresión regular
 		this.value = (this.value + '').replace(/[^0-9^+]/g, '');
	});
	//se valida que el campo de celular no se deje copiar o pegar
	jQuery('#edit-field-telefono-rutav-und-0-value').bind("cut copy paste",function(e) {
      	//se cancela la accion por defecto
      	e.preventDefault();
    });
    //se valida que el campo de confirmar celular no se deje copiar o pegar
	jQuery('#edit-field-telefono2-rutav-und-0-value').bind("cut copy paste",function(e) {
      	//se cancela la accion por defecto
      	e.preventDefault();
    });
});
Drupal.behaviors.form_submit_processor = {
  attach: function (context, settings) {
    jQuery("form#transfer-route-family-transfert-form").submit(function(e) {
    	//se captura la selcción
		var registro=jQuery("'[id*=edit-ruta-traslado-table-route-]':checked").val();
   		if(registro != undefined){
   			var cc = '';
   			jQuery('.form-checkbox:checked').each(function() {
		    	//se valida que el valor de el select no sea nulo
				var cedula_usuario = jQuery(this).val();
				if (cedula_usuario != 'on') {
					cc = cc + '<br>' + cedula_usuario + '<br>';
				}	
	   	    });
   			//se crea el aviso de confirmación para el proceso
   			alertify.confirm("Señor usuario los documentos que usted a seleccionado para el viaje son los siguientes : <br> "+ cc + "<br> Esta seguro de su selección ?",
            function (e) {
            	//se valida si el usuario acepta la condición
			    if (e) {
			    	//se captura el id del formulario
	          		var formulario =  document.getElementById('transfer-route-family-transfert-form');
	          		//se realiza el submit de los datos del formulario
	          		formulario.submit();
			    }
			    return false;
		    });
		    return false;	
   		}
    });
  }
}
