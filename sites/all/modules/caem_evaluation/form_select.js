jQuery(document).ready(function(){
	//se realiza la validación de selección de candidatos
	jQuery('#caem-evaluation-select-applicants-form').submit(function(event){
   		//se captura la selcción
   		var registro=jQuery('[id*=edit-table]').is(':checked');
   		//se valida el resultado de la selección
   		if (registro == false){
   			//si es false se envia mensaje al usuario
     		alertify.alert("Señor usuario deberá seleccionar al menos un candidato para poder continuar con la evaluación de competencias.");
     		//no se permite que el usuario continue
     		return false;
   		}else{
   			//funcion que permite captura el valor del get de la url
   			function getUrlVars() {
   				//variable que retorna el valor
				var vars = {};
				//se captura el valor dependiendo la key enviada
				var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
					//se asigna a vars el valor de la key
					vars[key] = value;
				});
				//se retorna el valor
				return vars;
			}
   			//se crea un arregĺo para guardar las cedulas de los usuarios
   			var datos_ch= new Array();
   			//se realiza un foreach para validar que datos estan seleccionados
   			jQuery('.form-checkbox:checked').each(
			    function() {
			    	//se valida que el valor de el select no sea nulo
					var cedula_usuario = jQuery(this).val();
					//se valida que el valor no sea de texto
					if(!isNaN(cedula_usuario)){
						//se almacenan la cedula del usuario en el array
						datos_ch.push(cedula_usuario);
					}
			    }
			);
			//se comvierte el array en un string separado por |
			var usuer_select = datos_ch.join('|');
			//se crea el aviso de confirmación para el proceso
   			alertify.confirm("Señor usuario recuerde que al continuar no podrá devolverse a seleccionar otros candidatos<br><br>Esta seguro de su selección?",
            function (e) {
            	//se valida si el usuario acepta la condición
			    if (e) {
			    	//se captura el valor get de identifiacion del usuario
			    	var cedula_evaluador =  getUrlVars()['id_user'];
			    	//se captura el valor get de la evaluación seleccionada
			    	var evaluac_select = getUrlVars()['evalua_user'];
			    	//se carga el id del nodo seleccionado por el usuario
			    	var nodo_seleccionado = getUrlVars()['user_id_sele'];
			    	//se decodifican los usuarios seleccionados
			    	var usuario_selecionado = btoa(usuer_select);
			    	//se realiaza el redireccionamiento al nuevo enlace
			    	window.location.replace('/caem_evaluation/confirm?ide_evalut='+cedula_evaluador+'&evalua_select='+evaluac_select+'&id_applicant='+usuario_selecionado+'&user_id_sele='+nodo_seleccionado);
			    }
			    //se valida si no acepta
			    else {
			    	//se presenta un mensaje de cancelación
				    alertify.error("Has pulsado <strong>"+ alertify.labels.cancel+ "</strong>");
		    	}
		    });
		    return false;
   		}
	});
    //validaciones para el boton finalizar de la evaluación para mostrar el campo de logros destacados
	jQuery('#edit-end-evaluacion-final').click(function() {
		alertify.prompt("Por favor elabore un concepto general y/o resalte un comportamiento/logro del candidato en su carrera militar.",
      	function(e, str) {

        	if (e) {
        		//se valida si el texto esta en blanco
        		var logro = !str.replace(/^\s+/g, '').length;
        		if (logro === true) {
        			alertify.alert("Señor usuario debe Ingresar por lo menos un logro");
        		}
        		else{
        			//se asigna el valor escrito a un campo oculto dentro del fomulario
	          		jQuery('#logros_evaluation').val(str);
	          		//se captura el id del formulario
	          		var formulario =  document.getElementById('caem-evaluation-perform-evaluation-form');
	          		//se realiza el submit de los datos del formulario
	          		formulario.submit();
        		}
        	}
        	else {
          		alertify.error("Has pulsado <strong>" + alertify.labels.cancel + "</strong>");
        	}
      	});
		return false;
	});
});