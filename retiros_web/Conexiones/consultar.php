<?php 
require_once('conexion.php');
error_reporting (0);
?>
<?php
$cedula = $_GET['usr_cc'];

/*$cedula='';
//se valida que exista get
$capturar_cedula=isset($_GET["usr_cc"])?$_GET["usr_cc"]:'';
//se valida que el datos cc exista
if($capturar_cedula != ''){
	$cedula = base64_decode($capturar_cedula);
}*/


$qbasico = "SELECT /* + INDEX(E EMPL_UK_IDX) */ E.GRAD_ALFABETICO GRADO,					
				   lower(E.NOMBRES||' '||E.APELLIDOS) NOMBRES,
				   (SELECT C.RV_ABBREVIATION
					  FROM CG_REF_CODES C
					 WHERE C.RV_DOMAIN = 'ESTADO CIVIL'
					   AND C.RV_LOW_VALUE = E.ESTADO_CIVIL) ESTADO_CIVIL,
				   to_char((SELECT MAX(P.FECHA_INICIO_TURNO)
					  FROM PLANES_VACACIONES_EMPLEADOS P
					 WHERE P.EMPL_CONSECUTIVO = E.CONSECUTIVO
					   AND P.EMPL_UNDE_FUERZA = E.UNDE_FUERZA
					   AND P.EMPL_UNDE_CONSECUTIVO = E.UNDE_CONSECUTIVO),'DD/MM/YYYY')VACACIONES,
				   (SELECT COUNT(BE.ID_BENEFICIARIO)
					  FROM BENEFICIARIOS_EMPLEADOS BE,BENEFICIARIOS B
					 WHERE BE.ID_PARENTESCO IN (4,5,108,139)
					   AND BE.EMPL_CONSECUTIVO = E.CONSECUTIVO
					   AND BE.EMPL_UNDE_FUERZA = E.UNDE_FUERZA
					   AND BE.EMPL_UNDE_CONSECUTIVO = E.UNDE_CONSECUTIVO
					   AND BE.ID_BENEFICIARIO = B.ID_BENEFICIARIO
					   AND BE.UNDE_FUERZA = B.FUERZA
					   AND FLOOR(MONTHS_BETWEEN(SYSDATE, B.FECHA_NACIMIENTO)/12) = 21) HIJOS,
				   (SELECT COUNT(T.HORAS_MILLAS)
					  FROM TIEMPOS_ACUMULADOS_REQUISITOS T,
						   CARGOS_EMPLEADOS C
					 WHERE T.TIPO_REQUISITO = 'HV'
					   AND T.ID_CARGO_EMPLEADO = C.ID_CARGO_EMPLEADO
					   AND C.EMPL_CONSECUTIVO = E.CONSECUTIVO
					   AND C.EMPL_UNDE_FUERZA = E.UNDE_FUERZA
					   AND C.EMPL_UNDE_CONSECUTIVO = E.UNDE_CONSECUTIVO) VUELO
           			,G.NUMERICO
           			,E.UNDE_CONSECUTIVO_LABORANDO ULABO
           			,E.UNDE_FUERZA
			  FROM EMPLEADOS E, GRADOS G
			 WHERE E.GRAD_ALFABETICO = G.ALFABETICO
	           AND E.UNDE_FUERZA = G.FUERZA
	           AND E.IDENTIFICACION = '".$cedula."'";

$stid = $conn->Execute($qbasico);
//oci_execute($stid);
//$row = ociexecute($stid,OCI_DEFAULT);
//var_dump($row);
//if($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)){
if(!$stid->EOF){
   $vgrado = $stid->fields['GRADO'];// $fields['GRADO'];
   $vnombre = $stid->fields['NOMBRES'];//$row['NOMBRES'];
   $vestado = $stid->fields['ESTADO_CIVIL'];//$row['ESTADO_CIVIL'];
   $vvac = $stid->fields['VACACIONES'];//$row['VACACIONES'];
   $vhijo = $stid->fields['HIJOS'];//$row['HIJOS'];
   $vvuelo = $stid->fields['VUELO'];//$row['VUELO'];
   $vnumeri = $stid->fields['NUMERICO'];//$row['NUMERICO'];
   $ulabora = $stid->fields['ULABO'];//$row['ULABO'];
   $vfuerza = $stid->fields['UNDE_FUERZA'];//$row['UNDE_FUERZA'];
}
$query = "SELECT /* + INDEX(E EMPL_UK_IDX,T) */i.descripcion,
				 to_char(t.fecha_inicio,'DD/MM/YYYY') F_INICIO,
				 decode(TO_CHAR(t.fecha_termino,'DD/MM/YYYY'),null,TO_CHAR(sysdate,'DD/MM/YYYY'),TO_CHAR(t.fecha_termino,'DD/MM/YYYY'))F_FIN,
				 caltiempo(t.fecha_inicio,decode(t.fecha_termino,null,sysdate,t.fecha_termino)) TOTAL
			FROM TIEMPOS_EMPLEADOS T
				,TIEMPOS           I
				,EMPLEADOS         E
		   WHERE E.CONSECUTIVO = T.EMPL_CONSECUTIVO
			 AND E.UNDE_FUERZA = T.EMPL_UNDE_FUERZA
			 AND E.UNDE_CONSECUTIVO = T.EMPL_UNDE_CONSECUTIVO
			 AND T.TIEM_ID_TIEMPO = I.ID_TIEMPO
			 AND T.TIEM_FUERZA = I.FUERZA
			 AND E.IDENTIFICACION = '".$cedula."'
			 order by t.fecha_inicio";

$queryb = "SELECT TMPSERVICIO(E.UNDE_CONSECUTIVO,E.UNDE_FUERZA,E.CONSECUTIVO) AS TIEMPO
			 FROM EMPLEADOS E
			WHERE E.IDENTIFICACION = '".$cedula."'";

$qtie = "SELECT TMPSERVICIO(E.UNDE_CONSECUTIVO,E.UNDE_FUERZA,E.CONSECUTIVO) ANTI
		   FROM EMPLEADOS E
		  WHERE E.IDENTIFICACION = '".$cedula."'";
$stid = $conn->Execute($qtie);
//$row = oci_execute($stid);
//if($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)){
if(!$stid->EOF){
   $vanios = $stid->fields['ANTI'];
}
$qbenefi ="SELECT /* + INDEX(E EMPL_UK_IDXE,B,P) */P.DESCRIPCION PARENTESCO,
				  B.NOMBRES||' '||B.APELLIDOS NOMBRES,
				  B.IDENTIFICACION DOCUMENTO,
				  to_char(B.FECHA_NACIMIENTO,'DD/MM/YYYY') F_NAC,
				  FLOOR(MONTHS_BETWEEN(SYSDATE, B.FECHA_NACIMIENTO)/12) EDAD
			 FROM EMPLEADOS E,
				  BENEFICIARIOS_EMPLEADOS BE,
				  BENEFICIARIOS B,
				  PARENTESCOS P
			WHERE E.CONSECUTIVO = BE.EMPL_CONSECUTIVO
			  AND E.UNDE_FUERZA = BE.EMPL_UNDE_FUERZA
			  AND E.UNDE_CONSECUTIVO = BE.EMPL_UNDE_CONSECUTIVO
			  AND B.ID_BENEFICIARIO = BE.ID_BENEFICIARIO
			  AND B.FUERZA = BE.UNDE_FUERZA
			  AND BE.ID_PARENTESCO = P.ID_PARENTESCO 
			  AND E.IDENTIFICACION = '".$cedula."'
		 ORDER BY B.FECHA_NACIMIENTO";

$qembargo = "SELECT /* + INDEX(E EMPL_UK_IDX,C) */C.DESCRIPCION ENTIDAD
				   ,B.FECHA_INICIO DESDE
				   ,B.FECHA_TERMINO HASTA
				   ,TO_CHAR(DECODE(B.VALOR_DEUDA,0,B.VALOR_MENSUAL,B.VALOR_DEUDA),'$999,999,999')MONTO
			   FROM EMPLEADOS     E
				   ,EMBARGOS_EMPL B
				   ,BANCOS_NOMINA C
			  WHERE E.CONSECUTIVO = B.EMPL_CONSECUTIVO
				AND E.UNDE_FUERZA = B.EMPL_UNDE_FUERZA
				AND E.UNDE_CONSECUTIVO = B.EMPL_UNDE_CONSECUTIVO
				AND B.ENTIDAD_BANCARIA = C.CODIGO
				AND E.IDENTIFICACION = '".$cedula."'";
$stem = $conn->Execute($qembargo);
//$row = oci_execute($stem);
//if($row = oci_fetch_array($stem, OCI_ASSOC+OCI_RETURN_NULLS)){
if(!$stem->EOF){
    $venti = $stem->fields['ENTIDAD'];
    $vdesde = $stem->fields['DESDE'];
    $vhasta = $stem->fields['HASTA'];
    $vmonto = $stem->fields['MONTO'];
}else{
	$venti = '';
}

$qciuda = "SELECT DISTINCT L.DESCRIPCION AS CIUDAD
			 FROM LUGARES_GEOGRAFICOS L
			WHERE L.TIPO NOT IN ('PA','CO','DE','IP','IPD','IPM','RIC','CAS','CG','ND','NR')
	   CONNECT BY PRIOR L.CODIGO = L.LUGE_PAPA
	   START WITH L.LUGE_PAPA = 10
		 ORDER BY L.DESCRIPCION";
//$row = oci_execute($sciu);

$qdpto = "SELECT DISTINCT L.CODIGO AS CODIGO, L.DESCRIPCION AS DPTO
			FROM LUGARES_GEOGRAFICOS L
		   WHERE L.TIPO IN ('DE')
			 AND L.LUGE_PAPA = 10
		ORDER BY L.DESCRIPCION";
//$row = oci_execute($sdpto);

$qjefe = "SELECT /* + INDEX(E EMPL_UK_IDX) */E.GRAD_ALFABETICO GRAJE
		        ,E.IDENTIFICACION CEDUJE
		        ,E.NOMBRES||' '||E.APELLIDOS NOMJE
		        ,E.EMAIL_INSTITUCIONAL MAILJE
		        ,(SELECT C.DESCRIPCION
			        FROM CARGOS C
			       WHERE C.CARGO = E.CARG_CARGO
			         AND C.FUERZA = E.CARG_FUERZA) CARJE
			    ,(SELECT S.SIGLA
			        FROM UNIDADES_DEPENDENCIA U
			            ,SIGLAS               S
			       WHERE U.CONSECUTIVO = E.UNDE_CONSECUTIVO_LABORANDO
			         AND U.FUERZA = E.UNDE_FUERZA_LABORANDO
			         AND U.ID_SIGLA = S.ID_SIGLA) UNIJE
		        ,E.CONSECUTIVO CONSEJE
		        ,E.UNDE_FUERZA FUERJE
		        ,E.UNDE_CONSECUTIVO UNCOJE
		    FROM EMPLEADOS E
		        ,GRADOS    G
		   WHERE E.UNDE_CONSECUTIVO_LABORANDO IN ('".$ulabora."')
		     AND E.ACTIVO = 'SI'
		     AND E.GRAD_ALFABETICO = G.ALFABETICO
		     AND E.UNDE_FUERZA = G.FUERZA
		     AND G.NUMERICO =
			       (SELECT MIN(NUMERICO)
			          FROM GRADOS    GG
			              ,EMPLEADOS EE
			         WHERE EE.GRAD_ALFABETICO = GG.ALFABETICO
			           AND EE.UNDE_FUERZA = GG.FUERZA
			           AND EE.ACTIVO = 'SI'
			           AND EE.UNDE_CONSECUTIVO_LABORANDO = E.UNDE_CONSECUTIVO_LABORANDO
			           AND EE.UNDE_FUERZA_LABORANDO = E.UNDE_FUERZA_LABORANDO)
		   ORDER BY G.NUMERICO
		         ,E.FECHA_ALTA_PROPIEDAD";
$sjefe = $conn->Execute($qjefe);
//$row = oci_execute($sjefe);
//if($row = oci_fetch_array($sjefe, OCI_ASSOC+OCI_RETURN_NULLS)){
if(!$sjefe->EOF){
   $vgraje = $sjefe->fields['GRAJE'];
   $vceduje = $sjefe->fields['CEDUJE'];
   $vnomje = $sjefe->fields['NOMJE'];
   $vmailje = $sjefe->fields['MAILJE'];
   $vcarje = $sjefe->fields['CARJE'];
   $vunije = $sjefe->fields['UNIJE'];
   $vconseje = $sjefe->fields['CONSEJE'];
   $vfuerje = $sjefe->fields['FUERJE'];
   $vuncoje = $sjefe->fields['UNCOJE'];
}

$qunisup = "SELECT U.UNDE_CONSECUTIVO PAPA
			  FROM UNIDADES_DEPENDENCIA U
			 WHERE U.CONSECUTIVO = '".$ulabora."'
			   AND U.FUERZA = 4";
$sunisu = $conn->Execute($qunisup);
//$row = oci_execute($sunisu);
//if($row = oci_fetch_array($sunisu, OCI_ASSOC+OCI_RETURN_NULLS)){
if(!$sunisu->EOF){
   $unipapa = $sunisu->fields['PAPA'];
}

$qpapa = "SELECT /* + INDEX(E EMPL_UK_IDX) */E.GRAD_ALFABETICO GRAPA
		        ,E.IDENTIFICACION CEDUPA
		        ,E.NOMBRES||' '||E.APELLIDOS NOMPA
		        ,E.EMAIL_INSTITUCIONAL MAILPA
		        ,(SELECT C.DESCRIPCION
			        FROM CARGOS C
			       WHERE C.CARGO = E.CARG_CARGO
			         AND C.FUERZA = E.CARG_FUERZA) CARPA
			    ,(SELECT S.SIGLA
			        FROM UNIDADES_DEPENDENCIA U
			            ,SIGLAS               S
			       WHERE U.CONSECUTIVO = E.UNDE_CONSECUTIVO_LABORANDO
			         AND U.FUERZA = E.UNDE_FUERZA_LABORANDO
			         AND U.ID_SIGLA = S.ID_SIGLA) UNIPA
		        ,E.CONSECUTIVO CONSEPA
		        ,E.UNDE_FUERZA FUERPA
		        ,E.UNDE_CONSECUTIVO UNCOPA
		    FROM EMPLEADOS E
		        ,GRADOS    G
		   WHERE E.UNDE_CONSECUTIVO_LABORANDO IN ('".$unipapa."')
		     AND E.ACTIVO = 'SI'
		     AND E.GRAD_ALFABETICO = G.ALFABETICO
		     AND E.UNDE_FUERZA = G.FUERZA
		     AND G.NUMERICO =
			       (SELECT MIN(NUMERICO)
			          FROM GRADOS    GG
			              ,EMPLEADOS EE
			         WHERE EE.GRAD_ALFABETICO = GG.ALFABETICO
			           AND EE.UNDE_FUERZA = GG.FUERZA
			           AND EE.ACTIVO = 'SI'
			           AND EE.UNDE_CONSECUTIVO_LABORANDO = E.UNDE_CONSECUTIVO_LABORANDO
			           AND EE.UNDE_FUERZA_LABORANDO = E.UNDE_FUERZA_LABORANDO)
		   ORDER BY G.NUMERICO
		         ,E.FECHA_ALTA_PROPIEDAD";
$spapa = $conn->Execute($qpapa);
//$row = oci_execute($spapa);
//if($row = oci_fetch_array($spapa, OCI_ASSOC+OCI_RETURN_NULLS)){
if(!$spapa->EOF){	
   $vgrapa = $spapa->fields['GRAPA'];
   $vcedupa = $spapa->fields['CEDUPA'];
   $vnompa = $spapa->fields['NOMPA'];
   $vmailpa = $spapa->fields['MAILPA'];
   $vcarpa = $spapa->fields['CARPA'];
   $vunipa = $spapa->fields['UNIPA'];
   $vconsepa = $spapa->fields['CONSEPA'];
   $vfuerpa = $spapa->fields['FUERPA'];
   $vuncopa = $spapa->fields['UNCOPA'];
}
?>