<?php require_once('conexion.php');?>
<?php require_once('consultar.php');?>
<?php error_reporting (0);?>
<?php

if (empty($_COOKIE["p_cedu"])) { 
	$cedre = $_COOKIE["p_ced"];
} elseif (empty($_COOKIE["p_ced"])) {
	$cedre = $_COOKIE["p_cedu"];
} 

$qcontenido = "SELECT /* + HINT(S,E,G,L,LL) */ S.CIU_SOLI
			         ,TO_CHAR(S.FECHASOL,'DD')||' de '|| TO_CHAR(S.FECHASOL,'FMMonth')||' de '||TO_CHAR(S.FECHASOL,'YYYY') FECHASOL
			         ,(CASE 
			           WHEN G.NUMERICO BETWEEN 1 AND 4 THEN 'JUAN MANUEL SANTOS CALDERON'
			           WHEN G.NUMERICO BETWEEN 5 AND 9 THEN 'LUIS CARLOS VILLEGAS'
			           ELSE 'LEONARDO SANTAMARIA GAITAN' END) A
			         ,(CASE 
			           WHEN G.NUMERICO BETWEEN 1 AND 4 THEN 'Presidente de la República'
			           WHEN G.NUMERICO BETWEEN 5 AND 9 THEN 'Ministro de Defensa'
			           ELSE 'Almirante' END) CON
			         ,(CASE 
			           WHEN G.NUMERICO BETWEEN 1 AND 4 THEN 'Casa de Nariño'
			           WHEN G.NUMERICO BETWEEN 5 AND 9 THEN 'Ministerio de Defensa Nacional'
			           ELSE 'Armada Nacional' END) EN
			         ,'Bogotá (Bogotá D.C.)' LUGAR
			         ,S.CONTENIDO
			         ,DECODE(S.FECHARET,NULL,'Acuerdo Expedición Acto Administrativo',S.FECHARET) FECHARET
			         ,E.DIRECCION
			         ,L.DESCRIPCION CIUDAD
			         ,LL.DESCRIPCION DPTO
			         ,E.TELEFONO
			         ,E.EMAIL
			         ,DECODE(S.COMUNICADO,1,'Si Acepto Ser Cumincado por Correo Electrónico',0,'No Acepto Ser Cumincado por Correo Electrónico',S.COMUNICADO) COMUNICADO
			         ,E.GRAD_ALFABETICO GRADO
			         ,E.NOMBRES||' '||E.APELLIDOS NOMBRES
			         ,E.IDENTIFICACION
			     FROM SOL_RETIROS_WEB S
			         ,EMPLEADOS E
			         ,GRADOS G
			         ,LUGARES_GEOGRAFICOS L
			         ,LUGARES_GEOGRAFICOS LL
			    WHERE E.IDENTIFICACION = S.CEDULARE
			      AND E.UNDE_FUERZA = S.UNDE_FUERZA
			      AND E.LUBA_LUGE_CODIGO = L.CODIGO
			      AND L.LUGE_PAPA = LL.CODIGO
			      AND E.GRAD_ALFABETICO = G.ALFABETICO
			      AND E.UNDE_FUERZA = G.FUERZA
			      AND S.VIGENTE = 1
			      AND S.CEDULARE = $cedre";	      
?>