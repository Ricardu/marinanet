<?php
require_once('Conexiones/consultar.php');
include('Conexiones/transa.php');
include ('fpdf/fpdf.php');
error_reporting (-1);

$cedula = $_GET['usr_cc'];

$var1 = $_SESSION['variable1'];
$var2 = $_SESSION['mes'];
$var3 = $_SESSION['variable3'];
$var4 = $_SESSION['variable2'];
if ($_SESSION['fecha'] == '') { 
	$var5 = "Acuerdo Expedición Acto Administrativo"; 
} else {
	$var5 = $_SESSION['fecha'];
}
$var6 = $_SESSION['variable6'];
$var7 = $_SESSION['variable8'];
$var8 = $_SESSION['departa'];
$var9 = $_SESSION['variable9'];
$var10 = $_SESSION['variable11'];
$var11 = $_SESSION['variable10'];

$solpdf = new FPDF('P','mm','Letter');
$solpdf -> Addpage();
#Establecemos los márgenes izquierda, arriba y derecha:
$solpdf->SetMargins(30, 25 , 30);
#Establecemos el margen inferior:
$solpdf->SetAutoPageBreak(true,25); 
$solpdf -> Ln(20);
$solpdf -> SetFont('Arial','',11);
$solpdf -> Cell(0,10,utf8_decode($var1).', '.utf8_decode($var2));
$solpdf -> Ln(20);
$solpdf -> SetFont('Arial','',11);
$solpdf -> Cell(0,10,utf8_decode('Señor'));
$solpdf -> Ln(4);
$solpdf -> SetFont('Arial','B',11);
$solpdf -> Cell(0,10,utf8_decode($var3));
$solpdf -> Ln(4);
$solpdf -> SetFont('Arial','',11);
if ($vnumeri <= 4) {
	$solpdf -> Cell(0,10,utf8_decode('Presidente de la República'));
	} elseif ($vnumeri > 4 && $vnumeri <= 9 ) {
		$solpdf -> Cell(0,10,utf8_decode('Ministro de Defensa'));
	}else {
		$solpdf -> Cell(0,10,utf8_decode('Almirante'));
}
$solpdf -> Ln(4);
if ($vnumeri <= 4) {
	$solpdf -> Cell(0,10,utf8_decode('Casa de Nariño'));
	} elseif ($vnumeri > 4 && $vnumeri <= 9 ) {
		$solpdf -> Cell(0,10,utf8_decode('Ministerio de Defensa Nacional'));
	}else {
		$solpdf -> Cell(0,10,utf8_decode('Armada Nacional'));
}
$solpdf -> Ln(4);
$solpdf -> Cell(0,10,utf8_decode('Bogotá (Bogotá D.C.)'));
$solpdf -> Ln(20);
$solpdf -> Cell(0,10,utf8_decode('Asunto: Solicitud Retiro Voluntario Armada Nacional.'));
$solpdf -> Ln(20);
$solpdf -> MultiCell(0,4,utf8_decode($var4),0);
$solpdf -> Ln(10);
$solpdf -> Cell(0,10,utf8_decode('Fecha de Retiro: ').utf8_decode($var5));
$solpdf -> Ln(15);
$solpdf -> Cell(0,10,utf8_decode('Recibo Comunicación en:  '));
$solpdf -> Ln(7);
$solpdf -> MultiCell(0,4,utf8_decode('Dirección: ').utf8_decode($var6).', '.utf8_decode($var7).', '.utf8_decode($var8),0,'J');
$solpdf -> Cell(0,4,utf8_decode('Teléfono: ').utf8_decode($var9));
$solpdf -> Ln(4);
$solpdf -> Cell(0,4,'Email: '.$var10);
$solpdf -> Ln(4);
$solpdf -> Cell(0,4,$var11.utf8_decode(' Acepto Ser Cumincado por Correo Electrónico'));
$solpdf -> Ln(15);
$solpdf -> Cell(0,10,utf8_decode('Respetuosamente;'));
$solpdf -> Ln(20);
$solpdf -> SetFont('Arial','B',11);
$solpdf -> Cell(0,10,$vgrado.' '.utf8_decode(mb_strtoupper($vnombre,'utf-8'))); 
$solpdf -> Ln(4);
$solpdf -> SetFont('Arial','',11);
$solpdf -> Cell(0,10,utf8_decode('Cedula de Ciudadanía ').$cedula);
$solpdf -> Output();
//include('mail/sendmail.php');
?>